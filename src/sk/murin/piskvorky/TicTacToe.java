package sk.murin.piskvorky;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TicTacToe implements ActionListener {
    private final JButton[] buttons = new JButton[9];
    private String player = "x";

    public TicTacToe() {
        JFrame frame = new JFrame("TicTacToe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridLayout(3, 3));

        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton();
            buttons[i].setName(String.valueOf(i));
            buttons[i].addActionListener(this);
            buttons[i].setFont(new Font("Consolas", Font.BOLD, 35));

            buttons[i].setFocusable(false);
            frame.add(buttons[i]);
        }

        frame.setResizable(false);
        frame.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        JButton buttonPressed = (JButton) e.getSource();
        String textFromBtn = buttonPressed.getText();


        if (!textFromBtn.isEmpty()) {
            return;
        }

        if (player.equals("x")) {
            player = "o";
            buttonPressed.setText(player);
        } else if (player.equals("o")) {
            player = "x";
            buttonPressed.setText(player);
        }
        if (check()) {
            JOptionPane.showMessageDialog(null, "Koniec! vyhral si", "-", JOptionPane.INFORMATION_MESSAGE);
            for (int i = 0; i < buttons.length; i++) {
                buttons[i].setEnabled(false);
            }
        } else {
            for (int i = 0; i < buttons.length; i++) {
                if (buttons[i].getText().isEmpty()) {
                    return;
                }
            }
            JOptionPane.showMessageDialog(null, "Remiza", "-", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private boolean check() {
        if (!buttons[0].getText().equals("") && buttons[0].getText().equals(buttons[1].getText())
                && buttons[1].getText().equals(buttons[2].getText())) {
            for (int i = 0; i < 3; i++) {
                buttons[i].setBackground(Color.green);
            }
            return true;
        }
        if (!buttons[3].getText().equals("") && buttons[3].getText().equals(buttons[4].getText())
                && buttons[4].getText().equals(buttons[5].getText())) {
            for (int i = 3; i < 6; i++) {
                buttons[i].setBackground(Color.green);
            }
            return true;
        }
        if (!buttons[6].getText().equals("") && buttons[6].getText().equals(buttons[7].getText())
                && buttons[7].getText().equals(buttons[8].getText())) {
            for (int i = 6; i < 9; i++) {
                buttons[i].setBackground(Color.green);
            }
            return true;
        }
        return false;
    }

}

